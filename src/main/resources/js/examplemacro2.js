jQuery(function ($) {
    var initExampleMacro2 = function ()
    {
        $(".exampleMacro2").each(function()
        {
        var dayDates = $(this).find("input.dayDates").val();
        var json;
        // determine if the browser has native JSON parser support & create JSON object
        if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function')
        {
            json = JSON.parse(decodeURIComponent(dayDates).replace(/\+/g, '\u00a0'));
        } else {
            json = eval('(' + decodeURIComponent(dayDates).replace(/\+/g, '\u00a0') + ')');
        }
        // create table
        var html = "<table border=\"1\"><tr><th>DAY</th><th>DATE</th></tr>";
        for (var i=0; i<7; i++)
        {
            html = html + "<tr><td>" + json.daysdates[i].day + "</td><td>" + json.daysdates[i].date + "</td></tr>";
        }
        html = html + "</table>";
        $(this).html(html);
    });
};
$(document).ready(function()
    {
        initExampleMacro2();
    });
});