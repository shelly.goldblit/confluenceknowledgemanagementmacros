jQuery(function ($) {
    var initTextTooltip = function ()
    {
        $(".text-tooltip-macro").each(function()
        {
            var direction = this.parentNode.computedStyleMap().get('direction').value
            this.style.direction=direction
        });
    };
$(document).ready(function()
    {
        initTextTooltip();
    });
});