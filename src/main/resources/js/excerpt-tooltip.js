jQuery(function ($) {
    var initExcerptTooltipMacro = function ()
    {
        $(".excerpt-tooltip-macro").each(function()
        {
            var direction = this.parentNode.computedStyleMap().get('direction').value
            //this.parentNode.style.direction = direction
            this.style.direction=direction
        });
};
$(document).ready(function()
    {
        initExcerptTooltipMacro();
    });
});