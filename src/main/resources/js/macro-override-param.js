var hideField = function (param, options) {
    return parameterField = AJS.MacroBrowser.ParameterFields["_hidden"](param, {});
     
}
var hiddenBodyMacroOverrides = {
    fields :
    { 
        "string" : {
            "bodyhidden": hideField,
            "bodydirhidden" :hideField
        }
    },
    beforeParamsSet : function (params) {
        if (params.bodyhidden)
        {
            var bodyDiv =  $("div.macro-body-div").find("textarea")
            if (bodyDiv.length > 0) {
                bodyDiv.val(params.bodyhidden.replace(/<br>/g, '\n'))
                if (params.bodydirhidden) {
                    bodyDiv[0].dir = params.bodydirhidden;
                }
            }

        }
		return params;
    },
	beforeParamsRetrieved : function (paramMap,macro, sharedParamMap) {
        var body = AJS.MacroBrowser.getMacroBody()
        paramMap.bodyhidden = body.replace(/\n/g, "<br>")
        if (paramMap.bodyhidden)
        {
            var bodyDiv =  $("div.macro-body-div").find("textarea")
            if (bodyDiv.length > 0) {
                paramMap.bodydirhidden = bodyDiv[0].dir
            }
        }
		return paramMap;
    }
}

var hiddenSpaceMacroOverrides = {
    fields :
    { 
        "string" : {
            "spacekeyhidden": hideField,
            "fullpagehidden": hideField
        }
    },
    beforeParamsSet : function (params) {
        // init parameters if empty
        params.page = params.fullpagehidden || AJS.params.pageTitle || ""
        params.fullpagehidden = params.fullpagehidden || AJS.params.spaceKey + ":"  + AJS.params.pageTitle
        params.spacekeyhidden = params.spacekeyhidden || AJS.params.spaceKey
        
        // // Check if page changed, save the new space key
        // if (!params.spacekeyhidden || 
        //     (params.fullpagehidden != params.page  &&
        //      !params.fullpagehidden.endsWith(params.page)))
        // {
        //     // Add spacespacekeyhidden key so macro works even when copying between spaces.
        //     params.spacekeyhidden = AJS.params.spaceKey
        // }
        // Remove space key prefix from page if in the same space 
        // in case macro was copied from another space.
        if (params.page.startsWith(AJS.params.spaceKey+":"))
        {
            params.page = params.page.split(":")[1];
        }
        // Add space key prefix from page if not in the same space 
        // in case macro was copied from another space.
        else if (!params.fullpagehidden.startsWith(AJS.params.spaceKey+":") &&
                !params.page.includes(":"))
                {
                    params.page = params.spacekeyhidden + ":" + params.page
                }
		return params;
    },
	beforeParamsRetrieved : function (paramMap,macro, sharedParamMap) {
        // if page is empty, set to current page
        if (!paramMap.page) {
            paramMap.page = AJS.params.pageTitle
            paramMap.spacekeyhidden = AJS.params.spaceKey
            paramMap.fullpagehidden = AJS.params.spaceKey + ":"  + AJS.params.pageTitle
        }
        else {
            // Add space key so macro works even when copying between spaces.
            paramMap.fullpagehidden = paramMap.page
            if (!paramMap.fullpagehidden.includes(":")) {
                paramMap.spacekeyhidden = AJS.params.spaceKey
                paramMap.fullpagehidden = paramMap.spacekeyhidden + ":"  + paramMap.page
            }
        }
		return paramMap;
    }
}
var hiddenPageMacroOverrides = {
    
    fields : {},
    beforeParamsSet : function (params) {
        params.page = AJS.params.pageTitle
		return params;
    },
	beforeParamsRetrieved : function (paramMap,macro, sharedParamMap) {
		return paramMap;
    }
}

var hideGridCardSizeMacroOverrides = {
    fields :
    { 
        "enum": {
            "size": hideField
        }
    } ,
    beforeParamsSet : function (params) {
        if (!params.size) {
            params.size = "small"
        } 
		return params;
    }
}

var hideImageCardTextBoxMacroOverrides = {
    fields :
    { 
        "enum" : {
            "text-box" : hideField
        }
    },  
    beforeParamsSet : function (params) {
        if (!params["text-box"]) {
            params["text-box"] = "bottom"
        } 
		return params;
    } 
}
// macroOverrides.fields["string"] = function(param)
// {
//     if (param.name.endsWith("hidden")) {
//                      var parameterField = AJS.MacroBrowser.ParameterFields["_hidden"](param, {});
//                      return parameterField;
//     }
// }

function mergeFields(fields1, fields2)
{
    var copyFields = function(to, from)
    {
        for (fieldtype in from)
        {
            // Can not merge functions
            if (typeof(from[fieldtype]) == "function" && !to[fieldtype] )
            {
                to[fieldtype] = from[fieldtype]
            }
            // Merge only fields list with fields list
            else if (typeof(from[fieldtype]) == "object" &&
                    (!to[fieldtype] || typeof(to[fieldtype]) == "object")) 
            {
                to[fieldtype] = to[fieldtype] || {}
                for (fieldname in from[fieldtype])
                {
                    to[fieldtype][fieldname] = from[fieldtype][fieldname]
                }
            } 
        }
    }
    var mergedFields = {}
    copyFields(mergedFields, fields1)
    copyFields(mergedFields, fields2)
    return mergedFields
}

function setSmartOverridesWithHiddenPage (macroName)
{
    //alert(macroName)
    //console.log('kuku')   
    AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage(macroName, ["png", "jpeg", "jpg", "gif"]);  
    var smartFieldsoverrides = AJS.MacroBrowser.getMacroJsOverride(macroName)
    // Hide page to select images from this page only.
    var origSmartFieldFunc =  smartFieldsoverrides.fields["confluence-content"] 
    smartFieldsoverrides.fields["confluence-content"] = function(param, options) {
        var field = origSmartFieldFunc(param,options)
        field.paramDiv.hide()
        field.paramDiv.hidden = true
        return field
    }
}
// Enable multiple overrides
function addMacroOverrides (macroName, macroOverrides)
{
    // Get original overrides
    var origOverrides = AJS.MacroBrowser.getMacroJsOverride(macroName)
    if (!origOverrides)
    {
        var initialEmptyOverrides={
            fields: {},
            beforeParamsRetrieved : function (paramMap, macro, sharedParamMap) {
                return paramMap
            },
            beforeParamsSet : function (params, insert) {
                return params
            }
        }
        AJS.MacroBrowser.setMacroJsOverride(macroName, initialEmptyOverrides)
        origOverrides = AJS.MacroBrowser.getMacroJsOverride(macroName)
    }
    // Merge fields
    origOverrides.fields = mergeFields(origOverrides.fields, macroOverrides.fields);

    // Save original methods
    if (!origOverrides.origBeforeParamsSet) {
        origOverrides.origBeforeParamsSet = [origOverrides.beforeParamsSet]
        origOverrides.beforeParamsSet = function (params, insert) {
            // Loop thrugh methods from previous overrides
            for (var i in origOverrides.origBeforeParamsSet)
            {
                params =origOverrides.origBeforeParamsSet[i](params, insert);
            }
            return params;
        }
    }
    if (macroOverrides.beforeParamsSet) {
        origOverrides.origBeforeParamsSet.push(macroOverrides.beforeParamsSet)
    }
    
    if (!origOverrides.origBeforeParamsRetrieved)
    {
        origOverrides.origBeforeParamsRetrieved = [origOverrides.beforeParamsRetrieved]
        origOverrides.beforeParamsRetrieved = function (paramMap,macro, sharedParamMap) {
            params = macroOverrides.beforeParamsRetrieved(paramMap, macro, sharedParamMap);
            // Loop thrugh methods from previous overrides
            for (var i in origOverrides.origBeforeParamsRetrieved)
            {
                params = origOverrides.origBeforeParamsRetrieved[i](paramMap, macro, sharedParamMap);
            }
            return params
                
        }
    }
    if (macroOverrides.beforeParamsRetrieved) {
        origOverrides.origBeforeParamsRetrieved.push(macroOverrides.beforeParamsRetrieved);
    }
}

addMacroOverrides("excerpt-tooltip", hiddenSpaceMacroOverrides);

addMacroOverrides("grid-text-card", hiddenBodyMacroOverrides);
addMacroOverrides("grid-text-card", hideGridCardSizeMacroOverrides)


//setSmartOverridesWithHiddenPage('grid-image-card');
AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("grid-image-card", ["png", "jpeg", "jpg", "gif"]); 
addMacroOverrides("grid-image-card", hiddenBodyMacroOverrides);
addMacroOverrides("grid-image-card", hiddenSpaceMacroOverrides);
addMacroOverrides("grid-image-card", hideGridCardSizeMacroOverrides)
addMacroOverrides("grid-image-card", hideImageCardTextBoxMacroOverrides)


//setSmartOverridesWithHiddenPage('hive-tile');
AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("hive-tile", ["png", "jpeg", "jpg", "gif"]); 
addMacroOverrides("hive-tile", hiddenSpaceMacroOverrides);


