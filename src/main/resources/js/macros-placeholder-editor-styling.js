
var updateMacro = function(macroNode, paramVal, paramName) {
  var $macroDiv = AJS.$(macroNode);
  AJS.Rte.getEditor().selection.select($macroDiv[0]);
  AJS.Rte.BookmarkManager.storeBookmark();
  var macroName = $(macroNode).attr("data-macro-name")

  if (macroName == "grid-image-card" || macroName == "grid-text-card") {
    var currentParams = Confluence.MacroParameterSerializer.deserialize($(macroNode).attr("data-macro-parameters"))
    currentParams[paramName] = paramVal
    
    //currentParams.text-box = param;
    var macroRenderRequest = {
        contentId: Confluence.Editor.getContentId(),
        macro: {
            name: macroName,
            params: currentParams,
            defaultParameterValue: "",
            body : ""
        }
    }
  }
  tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
  AJS.Rte.BookmarkManager.restoreBookmark();
};

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("big", function(e, macroNode) {
  updateMacro(macroNode, "big", "size");
});

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("small", function(e, macroNode) {
  updateMacro(macroNode, "small", "size");
});

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("wide", function(e, macroNode) {
  updateMacro(macroNode, "wide", "size");
});

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("long", function(e, macroNode) {
  updateMacro(macroNode, "long", "size");
});

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("full", function(e, macroNode) {
  updateMacro(macroNode, "full", "text-box");
});

AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("none", function(e, macroNode) {
  updateMacro(macroNode, "none", "text-box");
});
AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("overlay", function(e, macroNode) {
  updateMacro(macroNode, "overlay", "text-box");
});
AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("left", function(e, macroNode) {
  updateMacro(macroNode, "left", "text-box");
});
AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("right", function(e, macroNode) {
  updateMacro(macroNode, "right", "text-box");
});
AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("bottom", function(e, macroNode) {
  updateMacro(macroNode, "bottom", "text-box");
});

var SetMacroPlaceHolderStyles = function(ed)
{

  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=big"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=wide"]'+
    '{'+
      "width: 340px;" +
    "}")

  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=big"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=long"]'+
    '{'+
      "height: 340px;" +
    "}")

  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=small"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=long"]'+
    '{'+
      "width: 167px;" +
    "}")

  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=small"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="size=wide"]'+
    '{'+
      "height: 170px;" +
    "}")



  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=right"]' +
    '[data-macro-parameters*="size=big"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=right"]' +
    '[data-macro-parameters*="size=wide"]'+
    '{'+
      "border-left: #d9d9d9 170px solid;" + 
      "width: 170px;" +
    "}")
  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=left"]' +
    '[data-macro-parameters*="size=big"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=left"]' +
    '[data-macro-parameters*="size=wide"]'+
    '{'+
      "border-right: #d9d9d9 170px solid;" + 
      "width: 170px;" +
    "}")

  ed.dom.addStyle(
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=bottom"]' +
    '[data-macro-parameters*="size=big"],'+
    '*[data-macro-name*="grid-image-card"]' +
    '[data-macro-parameters*="text-box=bottom"]' +
    '[data-macro-parameters*="size=long"]'+
    '{'+
      "border-bottom: #d9d9d9 170px solid;" + 
      "height: 170px;" +
    "}")

    ed.dom.addStyle(
      '*[data-macro-name*="grid-image-card"]' +
      '[data-macro-parameters*="text-box=right"]' +
      '[data-macro-parameters*="size=small"],'+
      '*[data-macro-name*="grid-image-card"]' +
      '[data-macro-parameters*="text-box=right"]' +
      '[data-macro-parameters*="size=long"]'+
      '{'+
        "border-left: #d9d9d9 83.5px solid;" + 
        "width: 83.5px;" +
      "}")

    ed.dom.addStyle(
      '*[data-macro-name*="grid-image-card"]' +
      '[data-macro-parameters*="text-box=left"]' +
      '[data-macro-parameters*="size=small"],'+
      '*[data-macro-name*="grid-image-card"]' +
      '[data-macro-parameters*="text-box=left"]' +
      '[data-macro-parameters*="size=long"]'+
      '{'+
        "border-right: #d9d9d9 83.5px solid;" + 
        "width: 83.5px;" +
      "}")

      ed.dom.addStyle(
        '*[data-macro-name*="grid-image-card"]' +
        '[data-macro-parameters*="text-box=bottom"]' +
        '[data-macro-parameters*="size=small"],'+
        '*[data-macro-name*="grid-image-card"]' +
        '[data-macro-parameters*="text-box=bottom"]' +
        '[data-macro-parameters*="size=wide"]'+
        '{'+
          "border-bottom: #d9d9d9 85px solid;" + 
          "height: 85px;" +
        "}")

               // wait for https://bugs.chromium.org/p/chromium/issues/detail?id=109212 to be resolved, and then use clips from svg instaed
               // like so:
               //<img src="http://desktop-sltn12m:1990/confluence/download/attachments/819205/star-clusters-67616_640.jpg" width="340 " heigth="170" style="clip-path: url(http://desktop-sltn12m:1990/confluence/download/resources/com.sg.plugins.confluence.knowledge-management-macros/images/grid-image-macro.svg#overlay-clip);">

        ed.dom.addStyle(
          '*[data-macro-name*="grid-image-card"]' +
          '[data-macro-parameters*="text-box=none"]' +
          '{'+
            "clip-path: polygon(0 2%, 49% 51%,50% 49%, 1% 0, 97% 0, 49% 50%, 51% 51%, 100% 0%, 100% 100%, 50% 49%,49% 51%, 97% 100%, 0 100%, 51% 51%, 48% 51%, 0 97%);" + 
          "}")
        ed.dom.addStyle(
          '*[data-macro-name*="grid-image-card"]' +
          '[data-macro-parameters*="text-box=overlay"]' +
          '{'+
            "clip-path: polygon(0% 0%, 0% 100%, 46% 99%, 46% 46%, 54% 46%, 54% 54%, 46% 54%, 0 100%, 100% 100%, 100% 0%);" + 
          "}")
        
 


        ed.dom.addStyle(
          '[data-macro-name*="hive-tile"]' +
          '{'+
            "clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);" +
          "}")
}

AJS.bind("init.rte", function() {
 
  var ed = tinymce.activeEditor
  SetMacroPlaceHolderStyles(ed)
  
})


