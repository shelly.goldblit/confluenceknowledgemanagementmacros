package com.sg.plugins.confluence;

import java.util.Map;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.setup.settings.SettingsManager;

@Named ("HiveTile")
public class HiveTile extends KnowledgeManagmentBaseMacro implements EditorImagePlaceholder {

    private static final String TEMPLATE = "templates/hive-tile.vm";

	@ComponentImport
	private final SettingsManager settingsManager;
	@ComponentImport
	private final PageManager pageManager;
	@ComponentImport
	private final AttachmentManager attachmentManager;

	@Inject
	public HiveTile(SettingsManager settingsManager, PageManager pageManager, AttachmentManager attachmentManager) 
	{
	    this.settingsManager = settingsManager;   
		this.pageManager = pageManager;
    	this.attachmentManager = attachmentManager;
	}
	
	@Override
	public ImagePlaceholder getImagePlaceholder( final Map<String, String> params, final ConversionContext ctx) {
		String imgURL = this.getImageUrl(params,ctx, this.settingsManager, this.pageManager, this.attachmentManager);
		return new DefaultImagePlaceholder(imgURL, new Dimensions(150,150), false);
	}

	private String Trim(String str, Integer length)
	{
		if (str != null)
		{
			return ( str.length () > length ) ? str.substring ( 0 , length - 1 ).concat ( "…" ) : str;
		}
		return "";
	}
	@Override
	public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
		
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
		String link = params.get("link");
		// Truncate too long sub-title and title
		String text = Trim(params.get("sub-title"),40);
		String title = Trim(params.get("title"),25);
		
		
		if (link == null || link == "") {
			link = "#";
		}
		
		try {
			contextMap.put("img", getImageUrl(
                params,
                conversionContext, 
                this.settingsManager, 
                this.pageManager, 
                this.attachmentManager));
			
			contextMap.put("title", title);
            contextMap.put("sub-title", text);
			contextMap.put("link", link);
			return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
		}
		catch (Exception e) {
			throw new MacroExecutionException(e);
		}
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE ;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
