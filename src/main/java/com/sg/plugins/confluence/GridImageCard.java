package com.sg.plugins.confluence;

import java.util.Map;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.CustomHtmlEditorPlaceholder;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.setup.settings.SettingsManager;

@Named ("GridImageCard")
public class GridImageCard extends KnowledgeManagmentBaseMacro 
implements EditorImagePlaceholder 
//implements CustomHtmlEditorPlaceholder
{
//public class GridImageCard implements Macro {

    private static final String TEMPLATE = "templates/grid-image-card.vm";

	@ComponentImport
	private final SettingsManager settingsManager;
	@ComponentImport
	private final PageManager pageManager;
	@ComponentImport
	private final AttachmentManager attachmentManager;

	@Inject
	public GridImageCard(SettingsManager settingsManager, PageManager pageManager, AttachmentManager attachmentManager) 
	{
	    this.settingsManager = settingsManager;   
		this.pageManager = pageManager;
    	this.attachmentManager = attachmentManager;
	}
	
	private Dimensions getDimensions(String size)
	{
		int width =  170;
		int height = 170;
		if (size.contains("wide") || size.contains("big")) {
			width = 340;
		}
		if (size.contains("long") || size.contains("big")) {
			height = 340;
		}
		return new Dimensions(width, height);
		
	}

	
	private String getPlaceHolderImage(Map<String, String> params)
	{
		String size = params.get("size");
		String textPos = params.get("text-box");
		textPos = (textPos.contains("overlay") || textPos.contains("none")) ? "full" : textPos;
		String imageName = "image-" + size + "-" + textPos;
		return "/download/resources/com.sg.plugins.confluence.knowledge-management-macros:knowledge-management-macros/images/"+
		imageName +
		".png";
	}

	@Override
	public ImagePlaceholder getImagePlaceholder( final Map<String, String> params, final ConversionContext ctx) {
		
		String imgURL = this.getImageUrl(
                params,
                ctx, 
                this.settingsManager, 
                this.pageManager, 
                this.attachmentManager);
		//String imgURL = this.getPlaceHolderImage(params);
		//byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
		//String encodedUrl = Base64.getUrlEncoder().encodeToString(imgURL.getBytes());		
		Dimensions dimensions= this.getDimensions(params.get("size"));
		return new DefaultImagePlaceholder(imgURL, dimensions, false);
	}
	
	//@Override
	public Boolean applyPlaceholderChrome() 
	{
		return false;
	}

	//@Override
	public String getCustomPlaceholder(Map<String,String> parameters, String body, ConversionContext context) 
	{
		String imgURL = this.getImageUrl(
			parameters,
			context, 
			this.settingsManager, 
			this.pageManager, 
			this.attachmentManager);
		//String imgURL = this.getPlaceHolderImage(params);
		//byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
		//String encodedUrl = Base64.getUrlEncoder().encodeToString(imgURL.getBytes());		
		Dimensions dimensions= this.getDimensions(parameters.get("size"));
		String customHTML =  "<img" +
			" width=\"" + 
			dimensions.getWidth() + 
			"\" height=\"" +
			dimensions.getHeight() + 
			"\" src=\"" + 
			imgURL + 
			"\" usemap=\"#maptest\">" + 
			"<map name=\"maptest\">" + 
			"<area shape=\"rect\" coords=\"22,11,122,62\" href=\"map1.html\" alt=\"Blue\">" +
			"</map>" +
			"</img>";
		return customHTML;
	}

	@Override
	@RequiresFormat(Format.View)
	public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
		
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
		
		
		
		String size = params.get("size");
		String textBox = params.get("text-box");
		String body = params.get("bodyhidden");
		String direction = textBox;
		String arrow = "arrow-" + textBox;
		String link = params.get("link");
		String textDir = params.get("bodydirhidden");
		link = (link == null || link == "" ) ? "#" : link;
		textDir = (textDir== null || textDir == "") ? "ltr" : textDir;
		try {
			// Make text shorter if the image is not long or big
			//if (textBox.contains("overlay") &&
			//body = (size.contains("small") || size.contains("wide")) ? body.replaceAll("</?br[^>]*/?>", " ") : body;
			direction = (direction.contains("left") || direction.contains("right")) ? "vertical" : direction;
			direction = (direction.contains("top") || direction.contains("bottom")) ? "horizontal" : direction;
			contextMap.put("body", body );	
			contextMap.put("image", this.getImageUrl(params,conversionContext, this.settingsManager, this.pageManager, this.attachmentManager));
			contextMap.put("size",  size);
			contextMap.put("text-box", textBox);
			contextMap.put("direction", direction);
			contextMap.put("arrow-class", arrow);
			contextMap.put("title", params.get("title"));
			contextMap.put("link", link);
			contextMap.put("text-dir", textDir );
			//contextMap.put("color", params.get("color"));
			//contextMap.put("body", params.get("body"));
			return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
		}
		catch (Exception e) {
			throw new MacroExecutionException(e);
		}
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT ;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
