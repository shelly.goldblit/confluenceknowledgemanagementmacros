
package com.sg.plugins.confluence;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.setup.settings.SettingsManager;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;


public abstract class KnowledgeManagmentBaseMacro implements Macro {

    protected String sanitizeBody(String body, String macroName)
    {
        Document doc;
        String sanitizedBody = "";
       
        doc = Jsoup.parse(body);
        // get macros
        Elements macros = doc.select("." + macroName);
        for (Element macro : macros) {

            sanitizedBody += macro.toString();
        }
        return sanitizedBody;
    }
    
    protected String getPageName(Map<String, String> params, ConversionContext conversionContext)
	{
		return params.get("fullpagehidden").split(":")[1];
	}
    protected String getSpaceKey(Map<String, String> params, ConversionContext conversionContext)
	{
		return params.get("fullpagehidden").split(":")[0];
	}
    protected String getImageUrl(Map<String, String> params, 
                                ConversionContext conversionContext, 
                                SettingsManager settingsManager,
                                PageManager pageManager,
                                AttachmentManager attachmentManager)
        {
            // TODO add an "image not found" image
            String notFound= "/download/resources/com.sg.plugins.confluence.knowledge-management-macros/images/image-not-found.png";
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            
            String imageName = params.get("name");
            Page page = pageManager.getPage(getSpaceKey(params, conversionContext),getPageName(params, conversionContext));

            if (page != null)
            {
                //String url = this.attachmentManager.getAttachmentDownloadPath(space, imageName);
                Attachment image = attachmentManager.getAttachment(page, imageName);
                if (image != null)
                {
                    return baseUrl + image.getDownloadPathWithoutEncoding();
                }
            }
            return  baseUrl + notFound;
            
        }
    
}