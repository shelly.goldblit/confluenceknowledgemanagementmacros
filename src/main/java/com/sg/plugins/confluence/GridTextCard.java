package com.sg.plugins.confluence;

import java.util.Map;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.setup.settings.SettingsManager;

@Named ("GridTextCard")
public class GridTextCard extends KnowledgeManagmentBaseMacro implements EditorImagePlaceholder 
{

    private static final String TEMPLATE = "templates/grid-text-card.vm";

	@Inject
	public GridTextCard() 
	{
	}

	// private String getSize(Map<String, String> params) {
		
	// 	String size = params.get("size");
	// 	if (size == "wide") return "width:60%;";
	// 	if (size=="long") return "height:200%;";
	// 	return "";
				
	// }
	
	private Dimensions getDimensions(String size)
	{
		int width =  167;
		int height = 170;
		if (size.contains("wide") || size.contains("big")) {
			width = 340;
		}
		if (size.contains("long") || size.contains("big")) {
			height = 340;
		}
		return new Dimensions(width, height);
		
	}

	@Override
	public ImagePlaceholder getImagePlaceholder( final Map<String, String> params, final ConversionContext ctx) {
		String imgURL = "/download/resources/com.sg.plugins.confluence.knowledge-management-macros/images/card.png";
		//String imgURL = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAAqSURBVDhPY/zy9ft/BioBJihNFTBqGOlg1DDSwahhpINRw0gHVDSMgQEAiUAEBfvn/NwAAAAASUVORK5CYII=";
		//byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
		//String encodedUrl = Base64.getUrlEncoder().encodeToString(imgURL.getBytes());		
		Dimensions dimensions= this.getDimensions(params.get("size"));
		return new DefaultImagePlaceholder(imgURL, dimensions, false);
	}
	@Override
	public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
		
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();	
		String size = params.get("size");
		String body = params.get("bodyhidden");
		String link = params.get("link");
		String textDir = params.get("bodydirhidden");

		link = (link == null || link == "" ) ? "#" : link;
		textDir = (textDir== null || textDir == "") ? "ltr" : textDir;

        contextMap.put("body", body );	
        contextMap.put("size",  size);
        contextMap.put("title", params.get("title"));
		contextMap.put("link", link);
		contextMap.put("text-dir", textDir);
        return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
	}

	

	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT ;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
