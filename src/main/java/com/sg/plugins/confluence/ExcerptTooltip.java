package com.sg.plugins.confluence;

import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;

import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import java.util.ArrayList;
import com.atlassian.confluence.util.ExcerptHelper;

@Named ("ExcerptTooltip")
public class ExcerptTooltip extends KnowledgeManagmentBaseMacro {

    private static final String TEMPLATE = "templates/excerpt-tooltip.vm";

	@ComponentImport
	private final XhtmlContent xhtmlUtils;
	@ComponentImport
	private final PageManager pageManager;
	@ComponentImport
	private final SpaceManager spaceManager;
	// @ComponentImport
	// private final ExcerptHelper helper;

	@Inject
	public ExcerptTooltip( XhtmlContent xhtmlUtils, PageManager pageManager, SpaceManager spaceManager) 
	{
	    this.xhtmlUtils = xhtmlUtils;   
		this.pageManager = pageManager;
    	this.spaceManager = spaceManager;
		
	}

	@Override
	public String execute(Map<String, String> params, String bodyContent, ConversionContext ctx) throws MacroExecutionException
	{
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
		String pageName = params.get("fullpagehidden");
		contextMap.put("page", pageName);	
		contextMap.put("text", params.get("text"));
		
		Page page = this.pageManager.getPage(getSpaceKey(params, ctx),getPageName(params,ctx));
		if (page != null) {
			contextMap.put("url", page.getUrlPath());
		}
		else
		{
			contextMap.put("url", "#");
		}
		
		// if (page != null) {
		// 	String excerpt =  page.getExcerpt();
	    	
    	// 	contextMap.put("tooltip", excerpt);	
		// }
		// else
		// {
		// 	contextMap.put("tooltip", "KUKU");	
		// }
		
		
		// String excerpt;
		// try {
		// 	excerpt = this.xhtmlUtils.convertWikiToView(
		// 		"{excerpt-include:ds:page1|nopanel=true}",
		// 		conversionContext,
		// 		new ArrayList<RuntimeException>()
		// 	); 
		// }
		// catch (Exception e)
        // {
        //     throw new MacroExecutionException(e);
        // }
		
	    
    	String ret = VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    	return ret;
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
