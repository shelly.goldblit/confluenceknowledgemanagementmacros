package com.sg.plugins.confluence;

import java.util.Map;
import java.util.List;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import com.opensymphony.util.TextUtils;
 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.atlassian.confluence.json.json.JsonArray;
import com.atlassian.confluence.json.json.JsonObject;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.velocity.VelocityContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;
import javax.inject.Named;
@Named ("ExampleMacro2")
public class ExampleMacro2 implements Macro
{
  @ComponentImport
  private final PageManager pageManager;
  @ComponentImport
  private final SpaceManager spaceManager;
  
  @Inject
  public ExampleMacro2(PageManager pageManager, SpaceManager spaceManager)
  {
    this.pageManager = pageManager;
    this.spaceManager = spaceManager;
  }

  DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  DateFormat dayFormat = new SimpleDateFormat("EEEE");
  private static final String DAY_DATE_JSON = "dayDateJson";
  private static final String TEMPLATE = "templates/examplemacro2.vm";

  @Override
  public BodyType getBodyType()
  {
     return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType()
  {
     return OutputType.BLOCK;
  }
  @Override
  public String execute(Map<String, String> params, String body, ConversionContext conversionContext) 
                                                                      throws MacroExecutionException
  {
    Date startDate = getStartDateFromParams(params);
    JsonObject dayDateJsonObject = new JsonObject();
    JsonArray dayDateJsonArray = new JsonArray();
    for (int i=0; i < 7; i++)
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(startDate);
      cal.add(Calendar.DATE, i );
      Date date = cal.getTime();
      String day = dayFormat.format(date);
      dayDateJsonArray.add(nextDayDateJsonObject(day, date));
    }
    dayDateJsonObject.setProperty("daysdates", dayDateJsonArray);
    //Context contextMap = MacroUtils.createDefaultVelocityContext();
    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
    contextMap.put(DAY_DATE_JSON, GeneralUtil.urlEncode(dayDateJsonObject.serialize()));
    String ret = VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    return ret;

    // contextMap.put(DAY_DATE_JSON, GeneralUtil.urlEncode(dayDateJsonObject.serialize()));
    // return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
  }
  private Date getStartDateFromParams(Map params) throws MacroExecutionException
  {
    Date startDate = new Date();
    if (params.size() > 1)
    {
      try
      {
        startDate = dateFormat.parse((String) params.get("date"));
      }
      catch (ParseException e)
      {
        throw new MacroExecutionException("Unable to parse date");
      }
    }
    return startDate;
  }

  private JsonObject nextDayDateJsonObject(String day, Date date)
  {
    JsonObject nextDayDateJsonObject = new JsonObject();
    nextDayDateJsonObject.setProperty("date",date);
    nextDayDateJsonObject.setProperty("day", day);
    return nextDayDateJsonObject;
  }
}