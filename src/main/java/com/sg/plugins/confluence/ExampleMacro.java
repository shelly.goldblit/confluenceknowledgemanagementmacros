package com.sg.plugins.confluence;

import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.confluence.xhtml.api.XhtmlContent;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;

@Scanned
public class ExampleMacro implements Macro {
	
	private final XhtmlContent xhtmlUtils;

	//@Autowired
	public ExampleMacro(@ComponentImport XhtmlContent xhtmlUtils) 
	{
	    this.xhtmlUtils = xhtmlUtils;   
	}
	
	

	@Override
	
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
	    String body = conversionContext.getEntity().getBodyAsString();
	    final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
	    try
	    {
	        xhtmlUtils.handleMacroDefinitions(body, conversionContext, new MacroDefinitionHandler()
	        {
	            public void handle(MacroDefinition macroDefinition)
	            {
	                macros.add(macroDefinition);
	            }
	        });
	    }
	    catch (XhtmlException e)
	    {
	        throw new MacroExecutionException(e);
	    }
	    StringBuilder builder = new StringBuilder();
	    builder.append("<p>");
	    if (!macros.isEmpty())
	    {
	        builder.append("<table width=\"50%\">");
	        builder.append("<tr><th>Macro Name</th><th>Has Body?</th></tr>");
	        for (MacroDefinition defn : macros)
	        {
	            builder.append("<tr>");
	            builder.append("<td>").append(defn.getName()).append("</td><td>").append(defn.hasBody()).append("</td>");
	            builder.append("</tr>");
	        }
	        builder.append("</table>");
	    }
	    else
	    {
	        builder.append("You've done built yourself a macro! Nice work.");
	    }
	    builder.append("</p>");

	    return builder.toString();
	}
	

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}
