package com.sg.plugins.confluence;

import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;

@Named ("TextTooltip")
public class TextTooltip extends KnowledgeManagmentBaseMacro {

    private static final String TEMPLATE = "templates/text-tooltip.vm";

	@ComponentImport
	private final XhtmlContent xhtmlUtils;
	@ComponentImport
	private final PageManager pageManager;
	@ComponentImport
	private final SpaceManager spaceManager;

	@Inject
	public TextTooltip(XhtmlContent xhtmlUtils, PageManager pageManager, SpaceManager spaceManager) 
	{
	    this.xhtmlUtils = xhtmlUtils;   
		this.pageManager = pageManager;
    	this.spaceManager = spaceManager;
	}

	@Override
	public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
		
		// TODO : remove illegal html tags from text

	    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
    	contextMap.put("text", params.get("text"));
		contextMap.put("tooltip", params.get("tooltip"));	
    	String ret = VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    	return ret;
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
