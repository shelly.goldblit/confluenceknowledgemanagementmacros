package com.sg.plugins.confluence;

import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;

import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import java.util.ArrayList;
import com.atlassian.confluence.util.ExcerptHelper;

@Named ("Hive")
public class Hive extends KnowledgeManagmentBaseMacro {

    private static final String TEMPLATE = "templates/hive.vm";

	@ComponentImport
	private final XhtmlContent xhtmlUtils;
	

	@Inject
	public Hive( XhtmlContent xhtmlUtils) 
	{
	    this.xhtmlUtils = xhtmlUtils;   
	}

	@Override
	public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException
	{
		
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();		
		bodyContent = this.sanitizeBody(bodyContent, "hive-tile-macro");
		//System.out.println("******************");
        contextMap.put("body", bodyContent);	

    	String ret = VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    	return ret;
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}
