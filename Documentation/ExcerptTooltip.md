# Excerpt Tooltip

This macro displays an excerption content as a tooltip, thus allowing users to creae a terms dictionary.
Terms can displayed in multiple pages, but maintaned in one place.

![excerpt-tooltip](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/d5339144798f5cd22660cb9ea3cded60/excerpt-tooltip.gif)

_Screenshot 1: Excerpt tooltip in edit mode_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/ab5410ef2b4903bc42783e5429755963/image.png)

_Screenshot 2: The resulting tooltip_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/97d007fd8c0fd4f4e2638c44c8a37516/image.png)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![edit-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/2c8e86cd0b25b91b5a192b38b54b4799/edit-cheese.png)

## Parameters
Parameters are options that you can set to control the content or format of the macro output.
Where the parameter name used in Confluence storage format or wikimarkup is different to the label used in the macro browser, it will be listed below in brackets _(example)_.


| Parameter | Default | Description |
| ---       |  ------ |----------|
| **Text** <br> _(text)_  | none | Displayed text.
| **Page** <br> _(page)_ | none | Type the name of the page that contains the excerption to be displayed as tooltip content. You can use an excerption from a page in the same space or another space in the same wiki.<br>When you type the name of the page into the Hive Tile macro dialog, Confluence will offer a list of matching pages, including those from other spaces.<br>Alternatively, you can type the space key followed by a colon (:) and the page name, like this:<br>`SPACEKEY:Page name`



#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```

{excerpt-tooltip: page=Universe | text=universe }


```

**Macro name:** _excerpt-tooltip_

**Macro body:** None.

