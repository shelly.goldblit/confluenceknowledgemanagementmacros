# Hive Tile

This macro displays an image tile in a hive of images. 

![hive-tile](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/c98d4de3c43732957e48124e9865e5fd/hive-tile.gif)

#### Note! To use this macro properly you must first add a hive, and then add hive-tiles into the hive.

_Screenshot 1: Hive tile in the Hive macro placeholder_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/eff5f8e46de002836597daee453eeda5/image.png)

_Screenshot 2: The resulting Hive_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/71cf8786e020522f424499bc3ce05600/image.png)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![edit-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/2c8e86cd0b25b91b5a192b38b54b4799/edit-cheese.png)

## Parameters
Parameters are options that you can set to control the content or format of the macro output.
Where the parameter name used in Confluence storage format or wikimarkup is different to the label used in the macro browser, it will be listed below in brackets _(example)_.


| Parameter | Default | Description |
| ---       |  ------ |----------|
| **Title** <br> _(title)_  | none | Tile title.
| **Subtitle** <br> _(sub-title)_  | none | Tile subtitle.
| **Page** <br> _(page)_ | none | Type the name of the page that contains the image to be displayed. You can use an image from a page in the same space or another space in the same wiki.<br>When you type the name of the page into the Hive Tile macro dialog, Confluence will offer a list of matching pages, including those from other spaces.<br>Alternatively, you can type the space key followed by a colon (:) and the page name, like this:<br>`SPACEKEY:Page name`
| **Image** <br> _(name)_ | none |  Type the name of the image to be displayed.<br>When you type the name of the page into the Hive Tile macro dialog, Confluence will offer a list of matching images from the selected page.
| **Link** <br> _(link)_| # | Link address.



#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```
{hive}
{hive-tile: page=Universe | name=sky.png | title = title | sub-title = subtitle | link = #}
{hive-tile: page=Universe | name=star.png| title = title | sub-title = subtitle | link = #}
{hive}

```

**Macro name:** _hive-tile_

**Macro body:** None.

