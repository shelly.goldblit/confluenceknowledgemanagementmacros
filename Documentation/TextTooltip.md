# Text Tooltip

This macro displays text as a tooltip.

_Screenshot 1: Text tooltip in edit mode_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/cd33bcd91e19db9d49a209d07262fbe6/image.png)

_Screenshot 2: The resulting tooltip_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/b9ca53ff6dc95a8dc36b263de1a6f88f/image.png)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![edit-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/2c8e86cd0b25b91b5a192b38b54b4799/edit-cheese.png)

## Parameters
Parameters are options that you can set to control the content or format of the macro output.
Where the parameter name used in Confluence storage format or wikimarkup is different to the label used in the macro browser, it will be listed below in brackets _(example)_.


| Parameter | Default | Description |
| ---       |  ------ |----------|
| **Display Text** <br> _(text)_  | none | Displayed text.
| **Tooltip Text** <br> _(tooltip)_  | none | Tooltip text.




#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```

{text-tooltip: text=left | tooltip=The opposite of right }


```

**Macro name:** _text-tooltip_

**Macro body:** None.

