# Grid Text Card

This macro displays a text card in a grid a macro. 

![grid-text-card](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/77ecb9393498820575a2c9e7d5fbb4ce/image.png)

#### Note! To use this macro properly you must first add a grid, and then add grid-text-card into the grid.

_Screenshot 1: Grid text card in the grid macro placeholder_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/505b4a91d602580b8a3059727e035210/image.png)

_Screenshot 2: The resulting grid_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/ab29126510c71fe4a55a865a6bdfad74/image.png)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/49f98b5908dba47a6cb6c363d4f5ccfb/image.png)

## Parameters
Parameters are options that you can set to control the content or format of the macro output.
Where the parameter name used in Confluence storage format or wikimarkup is different to the label used in the macro browser, it will be listed below in brackets _(example)_.


| Parameter | Default | Description |
| ---       |  ------ |----------|
| **Title** <br> _(title)_  | none | Tile title.
| **Size** <br> _(size)_  | small | small, wide, long ot big.
| **Link**| # | Link address.



#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```
{grid}
{grid-text-card: title = Some Title | size = wide | link = #}
Text to display inside card
{grid-text-card}
{grid}

```

**Macro name:** _grid-text-card_

**Macro body:** Accepts rich text.

