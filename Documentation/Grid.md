# Grid Macro

This macro displays a masonry like responsive grid.

![grid](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/862151a16d8c6a143d269e84bcc375ae/grid.gif)


#### Note! To use this macro properly you must first add a grid, and then add grid-cards into the grid.


_Screenshot 1: Grid cards in the grid macro placeholder_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/4427346708b7e426cde035518b528b4b/image.png)

_Screenshot 2: The resulting grid_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/f79073d2e62df2c48e522bdea0eaa162/image.png)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![edit-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/2c8e86cd0b25b91b5a192b38b54b4799/edit-cheese.png)


## Parameters
No parameters


#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```
{grid}

```


**Macro name:** _grid_

**Macro body:** Accepts rich text (Note, that only grid cards will be rendered in view mode).

