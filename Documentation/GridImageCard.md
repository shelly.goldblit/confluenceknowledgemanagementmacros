# Grid Image Card

This macro displays an image card (with text) in a grid a macro. 

![grid-image-card](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/77ecb9393498820575a2c9e7d5fbb4ce/image.png)

#### Note! To use this macro properly you must first add a grid, and then add grid-image-card into the grid.

_Screenshot 1: Grid image card in the grid macro placeholder_

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/d3893af6fa3489147c777aade735198f/image.png)

_Screenshot 2: The resulting grid image card_

![grid-imag-card](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/4b77bf5a61a575d04ece2f2f3f7378dd/grid-imag-card.gif)

## Using this Macro

To add macro to a page:

1. From the editor toolbar, choose **Insert** ![insert-macro-icon](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/07093a342998f13a5d7b854aa6655cbb/insert-macro-icon.png) > Other Macros
2. Find and select the required macro
**Speed it up with autocomplete:** Type { and the beginning of the macro name, to see a list of suggested macros. In this example we're inserting the cheese macro.

![insert-cheese](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/1d31d3517bf5a70a37605b9b24bd5d74/insert-cheese.png)

To edit an existing macro: Click the macro placeholder and choose Edit. This will open the macro details, so you can edit the macro parameters.

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/c7633a3056f51313fecfa96ba11ba2df/image.png)

## Parameters
Parameters are options that you can set to control the content or format of the macro output.
Where the parameter name used in Confluence storage format or wikimarkup is different to the label used in the macro browser, it will be listed below in brackets _(example)_.


| Parameter | Default | Description |
| ---       |  ------ |----------|
| **Title** <br> _(title)_  | none | Tile title.
| **Page** <br> _(page)_ | none | Type the name of the page that contains the image to be displayed. You can use an image from a page in the same space or another space in the same wiki.<br>When you type the name of the page into the Hive Tile macro dialog, Confluence will offer a list of matching pages, including those from other spaces.<br>Alternatively, you can type the space key followed by a colon (:) and the page name, like this:<br>`SPACEKEY:Page name`
| **Image** <br> _(name)_ | none |  Type the name of the image to be displayed.<br>When you type the name of the page into the Hive Tile macro dialog, Confluence will offer a list of matching images from the selected page.
| **Text Box** <br> _(text-box)_| bottom | bottom, left, right, full, overlay or none.
| **Size** <br> _(size)_  | small | small, wide, long ot big.
| **Link** <br> _(link)_| # | Link address.


#### Wiki markup example

This example is useful when you want to add a macro outside the editor, for example as custom content in the sidebar, header or footer of a space.
```
{grid}
{grid-image-card: title = Some Title | size = wide  | page=Universe | name=sky.png | text-box=left | link = #}
Text to display inside card
{grid-image-card}
{grid}

```

**Macro name:** _grid-image-card_

**Macro body:** Accepts rich text.

