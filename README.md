# Knowledge Managment Macros for Confluence

As the majority of people online don’t read an article to the end, presentation has an important effect in making information easier to consume.
These macros were developed to help confluence users edit their pages with this in mind.

**All macros support being between pages and spaces.**


Supported versions: Confluence server 6.14 and further.

## This plugin features the following macros

### Visual aids

* Grid

![grid](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/862151a16d8c6a143d269e84bcc375ae/grid.gif)


This macro plays with a grid-focused layout to organize the assortment of information posted. 


* Hive

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/ede8f337eea62d780327437725e56581/image.png)


### Tooltips


* Excerpt Tooltip

This macro displays an excerption content as a tooltip, thus allowing users to creae a terms dictionary.
Terms can displayed in multiple pages, but maintaned in one place.

![excerpt-tooltip](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/d5339144798f5cd22660cb9ea3cded60/excerpt-tooltip.gif)


* Text Tooltip

![image](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/uploads/b9ca53ff6dc95a8dc36b263de1a6f88f/image.png)

## Installation Instructions

For a stable production release, please download the latest version in the [Releases tab](https://gitlab.com/shelly.goldblit/confluenceknowledgemanagementmacros/-/releases) and follow these instructions for install and configuration.

1. Log into your Confluence instance as an admin.
2. Click the admin dropdown and choose **Manage apps**.
3. Click on **Upload App**.\
<sub>   The _Upload app_ screen loads</sub>.
4. Click on **Choose File** and select the plugin file.
5. Click on **Upload**.
6. You're all set! \
<sub>   Click **Close** in the _Installed and ready to go_ dialog.</sub>


## Build Instructions

Download the source, and install Atllasian SDK (See [Attlassian documentation](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK)).


* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help  -- prints description for all commands in the SDK

